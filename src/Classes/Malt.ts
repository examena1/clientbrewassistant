export class Malt  {
    id?: string;
    malt:string;
    batchNr:string;
    parts:number;
    gram:number;


    constructor(id: string, malt: string, batchNr: string, parts: number, gram: number) {
        this.id = id;
        this.malt = malt;
        this.batchNr = batchNr;
        this.parts = parts;
        this.gram = gram;
    }

}
