export class MashVolume {
    id?: string;
    totalMalt: number;
    mashVolume:number
    lauteringWater:number;


    constructor(id: string, totalMalts: number, mashVolume: number, lauteringWater: number) {
        this.id = id;
        this.totalMalt= totalMalts;
        this.mashVolume = mashVolume;
        this.lauteringWater = lauteringWater;
    }
}
