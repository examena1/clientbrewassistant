import {IBrewFormObj} from "../Interface/IBrewFormObj";

export class User {
    id?: string;
    name: string;
    recipes:IBrewFormObj[];

    constructor(id: string, name: string, recipes: IBrewFormObj[]) {
        this.id = id;
        this.name = name;
        this.recipes = recipes;
    }
}
