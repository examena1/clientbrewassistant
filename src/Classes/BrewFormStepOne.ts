export class BrewFormStepOne {
    date:Date| null;
    style:string;
    name:string;
    batchNr:string;
    goalIbu:number| undefined;
    expectedGravity:number| undefined;


  constructor(date: Date | null, style: string, name: string, batchNr: string, goalIbu: number | undefined, expectedGravity: number | undefined) {
    this.date = date;
    this.style = style;
    this.name = name;
    this.batchNr = batchNr;
    this.goalIbu = goalIbu;
    this.expectedGravity = expectedGravity;
  }
}
