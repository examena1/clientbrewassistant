export class Volume {
    id?: string;
    breBoil:number;
    afterBoil:number;
    gravityAfterBoiling:number;
    newVolume:number

    constructor(id: string, breBoil: number, afterBoil: number, gravityAfterBoiling: number, newVolume: number) {
        this.id = id;
        this.breBoil = breBoil;
        this.afterBoil = afterBoil;
        this.gravityAfterBoiling = gravityAfterBoiling;
        this.newVolume = newVolume;
    }
}
