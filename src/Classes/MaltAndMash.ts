import { IMalt } from "../Interface/IMalt";
import {IMashVolume} from "../Interface/IMashVolume";

export class MaltAndMash {
    malt:IMalt;
    mash:IMashVolume;


    constructor(malt: IMalt, mash: IMashVolume) {
        this.malt = malt;
        this.mash = mash;
    }
}
