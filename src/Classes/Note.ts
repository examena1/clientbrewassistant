export class Note {
    id?: string;
    note:string

    constructor(id: string, note: string) {
        this.id = id;
        this.note = note;
    }
}
