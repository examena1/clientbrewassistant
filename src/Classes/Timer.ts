export class Timer {
    id?: string;
    color: string;
    title:string;
    time:number;
    index:number;


    constructor(id: string, color: string, title: string, time: number, index: number) {
        this.id = id;
        this.color = color;
        this.title = title;
        this.time = time;
        this.index = index;
    }
}
