export class Hops {
    id?: string;
    hops: string;
    batchNr: string;
    alphaAcid: number;
    parts: number;
    gram: number;


    constructor(id: string, hops: string, batchNr: string, alphaAcid: number, parts: number, gram: number) {
        this.id = id;
        this.hops = hops;
        this.batchNr = batchNr;
        this.alphaAcid = alphaAcid;
        this.parts = parts;
        this.gram = gram;
    }
}
