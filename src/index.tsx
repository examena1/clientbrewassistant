import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

import Amplify from 'aws-amplify'
import config from './aws-exports' //innehåller all info för kopplingen mellan aws och användare info
import '@aws-amplify/ui-react/styles.css'
import {AmplifyProvider} from '@aws-amplify/ui-react'
import {UserIdsContextProvider} from "./components/context/UserIdsContext";

Amplify.configure(config)


ReactDOM.render(
        <AmplifyProvider>
            <UserIdsContextProvider>
                <App/>
            </UserIdsContextProvider>
        </AmplifyProvider>
    ,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
