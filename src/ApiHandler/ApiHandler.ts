import {IUser} from "../Interface/IUser";



// Make the `request` function generic
// to specify the return data type:
export function request<TResponse>(
    url: string,
    // `RequestInit` is a type for configuring
    // a `fetch` request. By default, an empty object.
    config: RequestInit = {}

    // This function is async, it will return a Promise:
): Promise<TResponse> {

    // Inside, we call the `fetch` function with
    // a URL and config given:
    return fetch(url, config)
        // When got a response call a `json` method on it
        .then((response) => response.json())
        // and return the result data.
        .then((data) => data as TResponse);
}



export const apiHandler = {
    get: <TResponse>(url: string) => request<TResponse>(url),

    delete: (url: string) => request(url,{method:'DELETE'}),

    post: <T>(url: string, body:any) =>
        request<T>(url, { method: 'POST',  headers: { 'Content-Type': 'application/json' }, body: JSON.stringify(body)}),

    postPathVariable: <T>(url: string, ) =>
        request<T>(url, { method: 'POST'}),

    put: <T>(url: string, body:any) =>
        request<T>(url, { method: 'PUT',  headers: { 'Content-Type': 'application/json' }, body: JSON.stringify(body)}),


}
