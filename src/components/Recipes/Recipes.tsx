import React, {useEffect, useState} from "react";
import {
    Card,
    CardContent, Grid,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow, TextField
} from "@mui/material";
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";
import {useStyles} from "../../Style";
import {IBrewRecipe} from "../../Interface/IBrewRecipe";
import {apiHandler} from "../../ApiHandler/ApiHandler";
import Button from "@mui/material/Button";
import {useNavigate} from 'react-router-dom';

export const Recipes = () => {
    const classes = useStyles();
    const navigate = useNavigate();
    const [recipeList, setRecipeList] = useState<IBrewRecipe[]>([])
    const [id, setId] = useState<string>(JSON.parse(String(sessionStorage.getItem("user"))));
    const [show, setShow] = useState<boolean>(false);


    useEffect(() => {

    }, [])


    const toRecipe=(idString:string)=>{
        sessionStorage.setItem("listId", JSON.stringify(idString))
        navigate('/recipe');
    }

    const getList = () => {
        apiHandler.get<IBrewRecipe[]>('http://localhost:8080/recipe/all-recipes-from-user/' + id)
            .then(res => setRecipeList(res))
        setShow(true);
    }


    const deleteRecipe = (id: string) => {
        apiHandler.delete('http://localhost:8080/recipe/delete-recipe/' + id)
            .catch(i => console.log(JSON.stringify(i))).then(() => getList());
    }

    return (
        <Card>
            <Button onClick={getList}>Get List </Button>
            <CardContent className={classes.fontS}>
                {show ?
                    <TableContainer component={Paper}>
                        <Table aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                    <TableCell>Malt</TableCell>
                                    <TableCell align="right">Name</TableCell>
                                    <TableCell align="right">Style</TableCell>
                                    <TableCell align="right">BatchNr</TableCell>
                                    <TableCell align="right">Ibu</TableCell>
                                    <TableCell align="right">Date</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {recipeList.map((item, index) => (
                                    <TableRow
                                        onDoubleClick={()=>toRecipe(item.id)}
                                        key={index}>
                                        <TableCell component="th" scope="row">{item.beerValues.beerName}</TableCell>
                                        <TableCell align="right">{item.beerValues.beerStyle}</TableCell>
                                        <TableCell align="right">{item.beerValues.batchNr}</TableCell>
                                        <TableCell align="right">{item.beerValues.goalIbu}</TableCell>
                                        <TableCell align="left">{item.beerValues.date}</TableCell>
                                        <TableCell align="right"> <Button onClick={() => deleteRecipe(item.id)}>Delete
                                            recipe </Button></TableCell>
                                        {}
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer> : ""
                }
            </CardContent>
        </Card>
    )
}
