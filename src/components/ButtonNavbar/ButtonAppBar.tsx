import React, {FC, useEffect} from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import {Grid} from "@mui/material";
import useWindowSize from "../windowSize/useWindowSize";
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import {useNavigate} from "react-router-dom";
import {useAuthenticator} from "@aws-amplify/ui-react";


export const ButtonAppBar: FC = (props: any) => {
    const {width, height} = useWindowSize();
    const navigate = useNavigate();
    const {user, signOut} = useAuthenticator((context) => [context.user]);
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

    useEffect(() => {
        if(!JSON.parse(String(sessionStorage.getItem("recipe")))){
            navigate('/');
        }
    },[])

    const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };


    const sendHome = () => {
        navigate('/');
        handleClose();
    }
    const sendList = () => {
        navigate('/list');
        handleClose();
    }

    const logOut=()=>{
        sessionStorage.clear();
        handleClose();
        signOut();
    }
    return (

        <Box sx={{flexGrow: 1}}>
            <AppBar sx={{backgroundColor: 'white', color: '#7cb3cf', boxShadow: 0}} position="static">
                <Toolbar>
                    <IconButton
                        onClick={handleClick}

                        sx={{mr: 2}}
                    >
                        {width > 428 ? null : <MenuIcon  />}
                    </IconButton>
                        <Menu
                            id="simple-menu"
                            anchorEl={anchorEl}

                            open={Boolean(anchorEl)}
                            onClose={handleClose}
                        >
                            <MenuItem onClick={sendHome}>Home</MenuItem>
                            <MenuItem onClick={sendList}>Brew List</MenuItem>
                            <MenuItem onClick={logOut}>Logout</MenuItem>
                        </Menu>

                    <Grid
                        container
                        direction="row"
                        justifyContent="space-around"
                        alignItems="center"
                    >
                        {width < 428 ? <p>Have a great brew</p> :
                            <>
                                <Button sx={{color: '#7cb3cf', marginBottom: "5px"}} onClick={sendHome}>Home</Button>
                                <Button sx={{color: '#7cb3cf', marginBottom: "5px"}} onClick={sendList}>Brew list</Button>
                                <Button sx={{color: '#7cb3cf', marginBottom: "5px"}} onClick={logOut}>Sign out</Button>
                            </>
                        }
                    </Grid>

                </Toolbar>
            </AppBar>


        </Box>
    );
}
