import React from "react";

// export const Additives=[
//     {
//         Additive:'Lactose',
//         BatchNr: 'mo23s',
//         Property:'gives a thiker milky tast',
//         gram:300,
//
//     },
//     {
//         Additive:'zink',
//         BatchNr: 'm33',
//         Property:'gives a tast more remenistant of ale',
//         gram:0,
//
//     },
// ]

export const Malts=[
    {
        Malt:'Pilstner',
        BatchNr: 'An12m33',
        Parts:20,
        Gram:500,
    },
    {
        Malt:'Cara Bohemianr',
        BatchNr: 'A1056ad',
        Parts:40,
        Gram:1000,
    },
]
export const HoopsPlaceholder=[
    {
        Malt:'Amarillo',
        BatchNr: 'An---12m33',
        Alpha:14,
        Gram:4,
    },
    {
        Malt:'Citra',
        BatchNr: 'i--ua5',
        Alpha:4,
        Gram:10,
    },
]
