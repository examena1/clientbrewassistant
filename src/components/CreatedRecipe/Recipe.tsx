import React, {useEffect, useState} from 'react';
import {apiHandler} from "../../ApiHandler/ApiHandler";
import {IBrewRecipe} from "../../Interface/IBrewRecipe";

import {makeStyles} from '@material-ui/core/styles';
import {
    Card,
    Grid,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    TextField
} from "@mui/material";
import CardContent from '@material-ui/core/CardContent';
import {IMalt} from "../../Interface/IMalt";
import {IHops} from "../../Interface/IHops";
import {IAddetive} from "../../Interface/IAddetive";
import {IVolume} from "../../Interface/IVolume";
import {INote} from "../../Interface/INote";
import {ITimer} from "../../Interface/ITimer";
import {TimerChild} from "../NewBrewFrom/TimerChild";
import Button from "@mui/material/Button";
import {useNavigate} from 'react-router-dom';
import {IGravity} from "../../Interface/IGravity";

const useStyles = makeStyles({
    root: {
        minWidth: 275,
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
    bottomSpacing: {
        marginBottom: 12,
    },
    titleBeerName: {
        fontSize: '2.5vw',
        marginLeft: '45%',
        paddingBottom: 10,
        paddingTop: 10,
        fontStyle: 'oblique'
    },
    cardHeader: {
        marginLeft: '45%',
        fontSize: 30,
        fontStyle: 'oblique'
    }
});
export const Recipe = () => {
    const classes = useStyles();
    const [recipe, setRecipe] = useState<IBrewRecipe>();
    const [getMaltList, setMaltList] = useState<IMalt[]>([]);
    const [cheekTotalMaltFromList, setCheekTotalMaltFromList] = useState<number>(0);
    const [hopsList, setMainHopsList] = useState<IHops[]>([]);
    const [additiveList, setAdditiveList] = useState<IAddetive[]>([])
    const [volume, setVolume] = useState<IVolume>();
    const [note, setNote] = useState<INote>();
    const [timerList, setTimerList] = useState<ITimer[]>([])
    const [startTimer, setStartTimer] = useState<boolean>(false)
    const [id, setId] = useState<string>(JSON.parse(String(sessionStorage.getItem("recipe"))));
    const [gravity, setGravity] = useState<IGravity >({gravity: 0})
    const navigate = useNavigate();
    useEffect(() => {
        getRecipe()
        getMaltListApi()
        getHopsListFromApi()
        getVolumeListApi()
        getTimerListFromApi()
        getNoteApi()
        getAdditivesListFromApi()


    }, [])


    useEffect(() => {
        if(!JSON.parse(String(sessionStorage.getItem("recipe")))){
            navigate('/');
        }
        console.log(gravity);
    },[gravity])

    const getRecipe = () => {
        apiHandler.get<IBrewRecipe>('http://localhost:8080/recipe/recipe-by-id/' + id).then(data => setRecipe(data));
    }

    const getMaltListApi = () => {
        apiHandler.get<IMalt[]>('http://localhost:8080/malt/all-malts-from-recipe/' + id).then(malt => setMaltList(malt)).then(() => getTotalMaltFromMaltList());
    }
    const getTotalMaltFromMaltList = () => {
        apiHandler.get<number>('http://localhost:8080/malt/totalmaltgrams/' + id).then(malt => setCheekTotalMaltFromList(malt));
    }
    const getHopsListFromApi = () => {
        apiHandler.get<IHops[]>('http://localhost:8080/hops/all-hops-from-recipe/' + id).then(hops => setMainHopsList(hops));
    }
    const getAdditivesListFromApi = () => {
        apiHandler.get<IAddetive[]>('http://localhost:8080/additive/all-additives-from-recipe/' + id).then(additive => setAdditiveList(additive));
    }
    const getVolumeListApi = () => {
        apiHandler.get<IVolume>('http://localhost:8080/volume/' + id).then(vol => setVolume(vol));
    }
    const getTimerListFromApi = () => {
        apiHandler.get<ITimer[]>('http://localhost:8080/timer/all-timers-from-recipe/' + id).then(timer => setTimerList(timer));
    }

    const getNoteApi = () => {
        apiHandler.get<INote>('http://localhost:8080/recipe/get-note/' + id).then(vol => setNote(vol));
    }

    const startTimers = () => {
        setStartTimer(!startTimer ? true : false);

    };


    const  putGravityToApi=()=>{
            apiHandler.put<IGravity>('http://localhost:8080/volume/update_gravity/'+id,gravity).then(i=> console.log(i))
              .then(()=>getVolumeListApi()).catch(res=>res);
    }


    return (
        <Card>
            <div className={classes.titleBeerName}> {recipe?.beerValues.beerName}</div>
            <Grid container justifyContent="space-evenly">
                <Grid item xs={12} sm={6} md={4} lg={3} className={classes.bottomSpacing}>
                    <Grid container justifyContent="center">
                        <Card className={classes.root}>
                            <CardContent>
                                <div className={classes.cardHeader}>Beer values</div>
                                <TableContainer component={Paper}>
                                    <Table aria-label="simple table">
                                        <TableBody>
                                            <TableRow>
                                                <TableCell component="th" scope="row">Beer style</TableCell>
                                                <TableCell align="right">{recipe?.beerValues.beerStyle}</TableCell>
                                            </TableRow>
                                            <TableRow>
                                                <TableCell component="th" scope="row">Batch number</TableCell>
                                                <TableCell align="right">{recipe?.beerValues.batchNr}</TableCell>
                                            </TableRow>
                                            <TableRow>
                                                <TableCell component="th" scope="row">Goal IBU </TableCell>
                                                <TableCell align="right">{recipe?.beerValues.goalIbu}</TableCell>
                                            </TableRow>
                                            <TableRow>
                                                <TableCell component="th" scope="row">Expected Gravity</TableCell>
                                                <TableCell
                                                    align="right">{recipe?.beerValues.expectedGravity}</TableCell>
                                            </TableRow>
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                            </CardContent>
                        </Card>
                    </Grid>
                </Grid>
                <Grid item xs={12} sm={6} md={3} lg={3} className={classes.bottomSpacing}>
                    <Grid container justifyContent="center">
                        <Card className={classes.root}>
                            <div className={classes.cardHeader}>Malt</div>
                            <CardContent>
                                <TableContainer component={Paper}>
                                    <Table aria-label="simple table">
                                        <TableHead>
                                            <TableRow>
                                                <TableCell>Malt</TableCell>
                                                <TableCell align="right">Batch nr</TableCell>
                                                <TableCell align="right">Parts%</TableCell>
                                                <TableCell align="right">Gram</TableCell>
                                                <TableCell align="right">Total
                                                    Malt: {cheekTotalMaltFromList} </TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {getMaltList.map((item, index) => (
                                                <TableRow key={index}>
                                                    <TableCell component="th" scope="row">{item.malt}</TableCell>
                                                    <TableCell align="right">{item.batchNr}</TableCell>
                                                    <TableCell align="right">{item.parts}</TableCell>
                                                    <TableCell align="right">{item.gram}</TableCell>
                                                    <TableCell align="right">ww</TableCell>
                                                </TableRow>
                                            ))}
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                            </CardContent>

                        </Card>
                    </Grid>
                </Grid>
                <Grid item xs={12} sm={6} md={3} lg={3} className={classes.bottomSpacing}>
                    <Grid container justifyContent="center">
                        <Card className={classes.root}>
                            <div className={classes.cardHeader}>Hops</div>
                            <CardContent>
                                <TableContainer component={Paper}>
                                    <Table aria-label="simple table">
                                        <TableHead>
                                            <TableRow>
                                                <TableCell>Hops</TableCell>
                                                <TableCell align="right">Batch nr</TableCell>
                                                <TableCell align="right">Alpha acid%</TableCell>
                                                <TableCell align="right">Parts %</TableCell>
                                                <TableCell align="right">Hops grams: </TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {hopsList.map((item, index) => (
                                                <TableRow key={index}>
                                                    <TableCell component="th" scope="row">{item.hops}</TableCell>
                                                    <TableCell align="right">{item.batchNr}</TableCell>
                                                    <TableCell align="right">{item.alphaAcid}</TableCell>
                                                    <TableCell align="right">{item.parts}</TableCell>
                                                    <TableCell align="right">{item.gram}</TableCell>
                                                </TableRow>
                                            ))}
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                            </CardContent>
                        </Card>
                    </Grid>
                </Grid>
                <Grid item xs={12} sm={6} md={3} lg={3} className={classes.bottomSpacing}>
                    <Grid container justifyContent="center">
                        <Card className={classes.root}>
                            <div className={classes.cardHeader}>Additives</div>
                            <CardContent>
                                <TableContainer component={Paper}>
                                    <Table aria-label="simple table">
                                        <TableHead>
                                            <TableRow>
                                                <TableCell>Additive</TableCell>
                                                <TableCell align="right">Batch nr</TableCell>
                                                <TableCell align="right">Property</TableCell>
                                                <TableCell align="right">Gram</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {additiveList.map((item, index) => (
                                                <TableRow key={index}>
                                                    <TableCell component="th" scope="row">{item.additive}</TableCell>
                                                    <TableCell align="right">{item.batchNr}</TableCell>
                                                    <TableCell align="right">{item.property}</TableCell>
                                                    <TableCell align="right">{item.gram}</TableCell>
                                                </TableRow>
                                            ))}
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                            </CardContent>
                        </Card>
                    </Grid>
                </Grid>
                <Grid item xs={12} sm={6} md={3} lg={3} className={classes.bottomSpacing}>
                    <Grid container justifyContent="center">
                        <Card className={classes.root}>
                            <div className={classes.cardHeader}>Volume</div>
                            <CardContent>
                                <TableContainer component={Paper}>
                                    <Table aria-label="simple table">
                                        <TableBody>
                                            <TableRow>
                                                <TableCell component="th" scope="row">Pre-boil water</TableCell>
                                                <TableCell align="right">{volume?.breBoil}</TableCell>
                                            </TableRow>
                                            <TableRow>
                                                <TableCell component="th" scope="row">After-boil water </TableCell>
                                                <TableCell align="right">{volume?.afterBoil}</TableCell>
                                            </TableRow>
                                            <TableRow>
                                                <TableCell component="th" scope="row">Gravity after boil </TableCell>
                                                <TableCell align="right">{volume?.gravityAfterBoiling}</TableCell>
                                            </TableRow>
                                            <TableRow>
                                                <TableCell component="th" scope="row">New volume </TableCell>
                                                <TableCell align="right">{volume?.newVolume}</TableCell>
                                            </TableRow>
                                            <TextField sx={{marginLeft:'10px'}} id="standard-basic" label="Set gravity after boil" variant="standard"
                                                       onChange={e => setGravity(val=>({
                                                              ...val, gravity: parseInt(e.target.value)
                                                           })
                                                       )}
                                            />
                                                <Button onClick={putGravityToApi}>Add new gravity</Button>
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                            </CardContent>
                        </Card>
                    </Grid>
                </Grid>
                <Grid item xs={12} sm={6} md={3} lg={3} className={classes.bottomSpacing}>
                    <Grid container justifyContent="center">
                        <Card className={classes.root}>
                            <div className={classes.cardHeader}>Timer</div>
                            <CardContent>
                                <TableContainer component={Paper}>
                                    <Table aria-label="simple table">
                                        <TableHead>
                                            <TableRow>
                                                <TableCell>Time</TableCell>
                                                <TableCell align="right">title</TableCell>
                                                <TableCell align="right">Color</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {timerList.map((item, index) => (
                                                <TableRow key={index}>
                                                    <TableCell component="th" scope="row">{item.time/60}</TableCell>
                                                    <TableCell align="right">{item.title}</TableCell>
                                                    <TableCell align="right">{item.color}</TableCell>

                                                </TableRow>
                                            ))}
                                            {!startTimer ? <Button onClick={startTimers}>Start Timer schema</Button> :
                                                <Button onClick={startTimers}>Reset Timer Schema</Button>}
                                            {!startTimer ? "" : timerList.map((item, index) => (
                                                <Card key={index}

                                                >
                                                    <div className="flex-container">
                                                        <Grid container
                                                        >
                                                            <p>{item.title}: {index}</p>
                                                        </Grid>
                                                        <TimerChild setTime={item.time} label={"minuts"}
                                                                    color={item.color} index={index}/>

                                                    </div>
                                                </Card>
                                            ))
                                            }
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                            </CardContent>
                        </Card>
                    </Grid>
                </Grid>
                <Grid item xs={12} sm={6} md={3} lg={3} className={classes.bottomSpacing}>
                    <Grid container justifyContent="center">
                        <Card className={classes.root}>
                            <div className={classes.cardHeader}>Note</div>
                            <CardContent>
                                {note?.note}
                            </CardContent>
                        </Card>
                    </Grid>
                </Grid>
            </Grid>
        </Card>
    )
        ;
}
