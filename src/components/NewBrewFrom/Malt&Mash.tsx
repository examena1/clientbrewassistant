import React, {useContext, useEffect, useState} from 'react';
import {useStyles} from "../../Style";
import {
    Card,
    CardContent,
    Collapse,
    Grid,
    ListItemButton,
    ListItemText,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    TextField,
    Tooltip
} from "@mui/material";
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";
import {ExpandLess, ExpandMore} from "@material-ui/icons";
import {IMalt, IMaltPost} from "../../Interface/IMalt";
import {IMashVolume} from "../../Interface/IMashVolume";
import Button from "@mui/material/Button";
import {apiHandler} from "../../ApiHandler/ApiHandler";
import {IBrewFormObj} from "../../Interface/IBrewFormObj";
import {UserIdsContext} from "../context/UserIdsContext";

export const MaltMash = () => {
    const classes = useStyles();
    const [openMaltMash, setMaltMash] = React.useState(false);
    const [malt, setMalt] = useState<IMaltPost>({
        malt: "",
        batchNr: "",
        parts: 0,
    });
    const [getMaltList, setMaltList] = useState<IMalt[]>([]);
    const [show, setShow] = useState<boolean>(false);
    const [ mashAndVolume, setMashAndVolume] = useState<IMashVolume>({
        totalMalt: 0,
        mashVolume:0,
        lauteringWater:0,
    });

    const [cheekTotalMaltMashVolume,setCheekTotalMaltMashVolumeMashVolume] = useState<number>(0);
    const [cheekTotalMaltFromList,setCheekTotalMaltFromList] = useState<number>(0);

    const [id, setId] = useState<string>(JSON.parse(String(sessionStorage.getItem("recipe"))));


    const handleClickTwo = () => {
        setMaltMash(!openMaltMash);
    };

    const addMaltToList = () => {
         mashAndVolume.totalMalt<=0 ||cheekTotalMaltMashVolume<0?alert("Total Malt Gram must be given "):
        apiHandler.post<IMaltPost>('http://localhost:8080/malt/'+id,malt)
            .then(data=>console.log(data)).catch(res=>res).then(()=>getMaltListApi()).then(()=>getTotalMaltFromMaltList());
    }


    const sendMashValues=()=>{
        apiHandler.put<IMashVolume>('http://localhost:8080/mash/'+id,mashAndVolume)
            .then(data=>setCheekTotalMaltMashVolumeMashVolume(data.totalMalt))
            .then(()=>setShow(true))
            .catch(res=>res)
    }

    const getTotalMaltFromMaltList=()=>{
        apiHandler.get<number>('http://localhost:8080/malt/totalmaltgrams/'+id).then(malt=>setCheekTotalMaltFromList(malt));
    }
    const getMaltListApi=()=>{
        apiHandler.get<IMalt[]>('http://localhost:8080/malt/all-malts-from-recipe/'+id).then(malt=>setMaltList(malt)).then(()=>getTotalMaltFromMaltList());
    }
    const deleteMalts=(id:string)=>{
        apiHandler.delete('http://localhost:8080/malt/'+id).catch(i=>i).then(()=>getMaltListApi()).then(()=>getTotalMaltFromMaltList());
    }
    return (

        <>
            <ListItemButton onClick={handleClickTwo}>
                <img className={classes.iconImage} src={require("../../icons/3656879.png")} alt=""/>
                <ListItemText className={classes.tabDropdownMenuTitle} primary="Malt & Mash"/>
                {openMaltMash ? <ExpandLess/> : <ExpandMore/>}
            </ListItemButton>
            <Collapse in={openMaltMash} timeout="auto" unmountOnExit>
                <Card sx={{minWidth: 275}}>
                    <Grid
                        container
                        direction="row"
                        justifyContent="space-around"
                        alignItems="center"
                        sx={{marginBottom:'10px'}}
                    >

                        <Grid item xs={3}>
                            <TextField id="standard-basic" label="Total Malt gram" variant="standard"
                                       onChange={e => setMashAndVolume(val => ({...val,totalMalt: parseInt(e.target.value)}))}
                            />
                        </Grid>
                        <Grid item xs={3}>
                            <Tooltip title="Rekomended 2.5l per 1000g malt">
                                <TextField id="standard-basic" label="Mash water liter" variant="standard"
                                           onChange={e => setMashAndVolume(val => ({...val,mashVolume: parseInt(e.target.value)}))}
                                />
                            </Tooltip>
                        </Grid>
                        <Grid item xs={3}>
                            <TextField id="standard-basic" label="Lautering water liter" variant="standard"
                                       onChange={e => setMashAndVolume(val => ({...val,lauteringWater: parseInt(e.target.value)}))}
                            />
                        </Grid>
                        <Button  onClick={sendMashValues} sx={{color:'#7cb3cf',  marginBottom:"5px"}} >Send Mash Values </Button>
                    </Grid>

                    {show?
                    <CardContent className={classes.fontS}>
                        <TableContainer component={Paper}>
                            <Table aria-label="simple table">
                                <TableHead>
                                    <TableRow>
                                        <TableCell>Malt</TableCell>
                                        <TableCell align="right">Batch nr</TableCell>
                                        <TableCell align="right">Parts%</TableCell>
                                        <TableCell align="right">Gram</TableCell>
                                        <TableCell align="right">Total
                                            Malt {cheekTotalMaltFromList > cheekTotalMaltMashVolume ? "Exided Total malt" : cheekTotalMaltFromList}</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {getMaltList.map((item, index) => (
                                        <TableRow
                                            onDoubleClick={() => deleteMalts(item.id)}
                                            key={index}>
                                            <TableCell component="th" scope="row">{item.malt}</TableCell>
                                            <TableCell align="right">{item.batchNr}</TableCell>
                                            <TableCell align="right">{item.parts}</TableCell>
                                            <TableCell align="right">{item.gram}</TableCell>
                                            <TableCell align="right">{""}</TableCell>{/*Denna tableCell använde jag för att skåpa spcing orkade inte använda css men annars så vet jag att detta är inte snyggt */}
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </TableContainer>
                        <Grid
                            className={classes.marginForTables}
                            container
                            direction="row"
                            justifyContent="space-around"
                            alignItems="center"
                        >
                            <Grid item xs={2}>
                                <TextField id="standard-basic" label="Malt" variant="standard"
                                           onChange={e => setMalt(val => ({...val, malt: e.target.value}))}/>
                            </Grid>
                            <Grid item xs={2}>
                                <TextField id="standard-basic" label="Batch nr" variant="standard"
                                           onChange={e => setMalt(val => ({...val, batchNr: e.target.value}))}/>
                            </Grid><Grid item xs={2}>
                            <TextField id="standard-basic" label="Parts %" variant="standard"
                                       onChange={e => setMalt(val => ({...val, parts: parseInt(e.target.value)}))}/>
                        </Grid>
                            <Grid item xs={0}>
                                < AddCircleOutlineIcon className="fa fa-plus-circle marginTop" sx={{color: '#3d84a8'}}
                                                       onClick={addMaltToList}/>
                            </Grid>

                        </Grid>
                    </CardContent>:""
                    }
                </Card>
            </Collapse>
        </>
    )
}
