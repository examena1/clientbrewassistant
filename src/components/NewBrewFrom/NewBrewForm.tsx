import React, {useState} from 'react';
import logo from "../../images/two.png";
import altLogo from "../../images/one.png";
import {useStyles} from "../../Style";
import {DesktopDatePicker} from "@mui/lab";
import {FormControl, Grid, InputLabel, MenuItem, Select, TextField} from "@mui/material";
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import {ExtraAdditives} from "./ExtraAdditives";
import {MaltMash} from "./Malt&Mash";
import {Hops} from "./Hops";
import {Timer} from "./Timer";
import {Note} from "./Note";
import {IBrewFormObj} from "../../Interface/IBrewFormObj";
import Button from "@mui/material/Button";
import {apiHandler} from "../../ApiHandler/ApiHandler";
import {useNavigate} from 'react-router-dom';


export const NewBrewForm = () => {
    const navigate = useNavigate();
    const classes = useStyles();
    const [object,setObject] = useState<IBrewFormObj> ({
        batchNr: "",
        expectedGravity: 0,
        goalIbu: 0,
        beerName:"",
        date:new Date(),
        beerStyle:""
    })
    {/* denna lista är tänkt ska hämtas från api kallelse backend, kommer ändras i framtiden  */}
    const [valBeerStyles,setVal]=useState<string[]>(["Lager","Ipa","Ale","Stout","Wheat Beer","Other"])

    const [showList,setShowList]=useState<boolean>(false)
    const [id, setId] = useState<string>(JSON.parse(String(sessionStorage.getItem("recipe"))));



    const createRecipeBluePrintAndShowChildComponents =()=>{
        setShowList(showList===false?true:false)

        apiHandler.put<IBrewFormObj>('http://localhost:8080/beer-values/update/'+id,object)
        .then(data=>console.log(data)).catch(res=>res)

    }

    const toNewRecipe=()=>{
        navigate('/recipe');
    }



    return (
        <div>
            <Grid
                container
                direction="row"
                justifyContent="space-around"
                alignItems="center"
            >
                <Grid item xs>
                    <img className={classes.imgNewBrewForm} src={logo} alt={altLogo}/>
                </Grid>

                {/*//datepicker finns för om någon vill skapa ett recept en dag men vill brygga vid ett annat tillfälle*/}
                <Grid style={{paddingRight: '20px'}} item xs={7}>
                    <div>
                        <LocalizationProvider dateAdapter={AdapterDateFns}>
                            <DesktopDatePicker
                                inputFormat="MM/dd/yyyy"
                                className={classes.datepickerButton}
                                label="Brew Date"
                                value={object.date}
                                onChange={e=>setObject(val =>({
                                    ...val,
                                    date:e,
                                }))}
                                renderInput={(params) => <TextField {...params} />}
                            />
                        </LocalizationProvider>
                    </div>
                </Grid>
            </Grid>
            <Grid
                container
                direction="row"
                justifyContent="space-around"
                alignItems="center"
            >
                <Grid item xs={5}>
                    <TextField id="standard-basic" label="Beer name" variant="standard"   onChange={e=>setObject(val =>({
                        ...val,
                        beerName:e.target.value,
                    }))}/>
                </Grid>
                <Grid item xs={5}>
                    <TextField id="standard-basic" label="Brew batch" variant="standard"   onChange={e=>setObject(val =>({
                        ...val,
                        batchNr:e.target.value,
                    }))}/>
                </Grid>
            </Grid>

            <Grid
                container
                direction="row"
                justifyContent="space-around"
                alignItems="center"
                sx={{marginBottom: '10px'}}
            >
                <Grid item xs={12}>
                    <FormControl sx={{m: 1, minWidth: 120, marginLeft: '4.3%'}}>
                        <InputLabel>Beer Style</InputLabel>
                        <Select
                            labelId="demo-simple-select-helper-label"
                            id="demo-simple-select-helper"
                            value={object.beerStyle}
                            label="Beer-Style"
                            onChange={e=>setObject(val =>({
                                ...val,
                               beerStyle:e.target.value,
                            }))}
                        >
                            {valBeerStyles.map(item=>{
                                return   <MenuItem value={item}>{item}</MenuItem>
                                })
                            }

                        </Select>
                    </FormControl>
                </Grid>
                <Grid item xs={5}>
                    <TextField
                        id="outlined-number"
                        label="Goal IBU"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        onChange={e=>setObject(val =>({
                            ...val,
                         goalIbu: parseInt(e.target.value),
                        }))}
                    />
                </Grid>
                <Grid item xs={5}>
                    <TextField
                        id="outlined-number"
                        label="Expected Gravity Plato"
                        onChange={e=>setObject(val =>({
                            ...val,
                            expectedGravity:parseInt(e.target.value),
                        }))}
                        InputLabelProps={{
                            shrink: true,
                        }}
                    />
                </Grid>
                <Button onClick={createRecipeBluePrintAndShowChildComponents}  sx={{color:'#7cb3cf',  marginBottom:"5px"}} >Send Values </Button>
            </Grid>
            {
                showList?<div>
                        <MaltMash/>
                        <Hops/>
                        <ExtraAdditives/>
                        {/*<ExpectedVolumeResults/>*/}
                        <Timer/>
                        <Note/>
                        <Grid item xs={11}>
                            <Button
                                onClick={toNewRecipe}
                                variant="outlined" sx={{color:'#7cb3cf', border: '1px solid #7cb3cf', marginBottom:"5px",marginLeft:"5%", marginTop:'10px'}} >Create Recipe</Button>
                        </Grid>
                    </div>
                :""
            }
        </div>
    )
}
