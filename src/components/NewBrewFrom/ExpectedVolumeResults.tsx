import React, {useContext, useEffect, useState} from "react";
import {useStyles} from "../../Style";
import {
    Card,
    CardContent,
    Collapse,
    ListItemButton,
    ListItemText,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableRow
} from "@mui/material";
import {ExpandLess, ExpandMore} from "@material-ui/icons";
import {IVolume} from "../../Interface/IVolume";
import {apiHandler} from "../../ApiHandler/ApiHandler";
import {IMalt} from "../../Interface/IMalt";
import {UserIdsContext} from "../context/UserIdsContext";

export const ExpectedVolumeResults = () => {
    const classes = useStyles();
    const [openExpectedVolume, setExpectedVolume] = React.useState(false);
    const [volume, setVolume] = useState<IVolume>();
    const [id, setId] = useState<string>(JSON.parse(String(sessionStorage.getItem("recipe"))));


    const handleClickFour = () => {
        setExpectedVolume(!openExpectedVolume);
    };

    useEffect(() => {
      getVolumeApi();
    }, [])



    const getVolumeApi = () => {
        apiHandler.get<IVolume>('http://localhost:8080/volume/' + id).then(vol => setVolume(vol));
    }

    return (
        <>
            <ListItemButton onClick={handleClickFour}>
                <img className={classes.iconImage} src={require("../../icons/2792636.png")} alt=""/>
                <ListItemText className={classes.tabDropdownMenuTitle} primary="Volume"/>
                {openExpectedVolume ? <ExpandLess/> : <ExpandMore/>}
            </ListItemButton>
            <Collapse in={openExpectedVolume} timeout="auto" unmountOnExit>
                <Card sx={{minWidth: 275}}>

                    <CardContent className={classes.fontS}>
                        <TableContainer component={Paper}>
                            <Table aria-label="simple table">
                                <TableBody>
                                    <TableRow>
                                        <TableCell component="th" scope="row">Pre-boil water</TableCell>
                                        <TableCell align="right">{volume?.breBoil}</TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell component="th" scope="row">After-boil water </TableCell>
                                        <TableCell align="right">{volume?.afterBoil}</TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell component="th" scope="row">Gravity after boil </TableCell>
                                        <TableCell align="right">{volume?.gravityAfterBoiling}</TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell component="th" scope="row">New volume </TableCell>
                                        <TableCell align="right">{volume?.newVolume}</TableCell>
                                    </TableRow>
                                </TableBody>
                            </Table>
                        </TableContainer>

                    </CardContent>
                </Card>
            </Collapse>
        </>
    );
}
