import React, {useContext, useEffect, useState} from 'react';
import {Card, CardContent, Collapse, Grid, ListItemButton, ListItemText, TextField} from "@mui/material";
import {ExpandLess, ExpandMore} from "@material-ui/icons";
import {useStyles} from "../../Style";
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";
import {apiHandler} from "../../ApiHandler/ApiHandler";
import {INote} from "../../Interface/INote";


export const Note = () => {
    const classes = useStyles();
    const [value, setValue] = React.useState<string>();
    const [openNote, setNote] = React.useState(false);
    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setValue(event.target.value);
    };
    const handleClickSix = () => {
        setNote(!openNote);
    };

    const [recipeNote,setRecipe ]=useState<INote>();
    const [id, setId] = useState<string>(JSON.parse(String(sessionStorage.getItem("recipe"))));



    const  postNoteToApi=()=>{

        apiHandler.put<INote>('http://localhost:8080/recipe/note/'+id,value)
            .then(response => setRecipe(response))
            .catch(data =>
                console.log(data)
            );
    }



    return(
      <>
      <ListItemButton onClick={handleClickSix}>
          <img   className={classes.iconImage }src={require("../../icons/note.png")} alt=""/>
          <ListItemText className={classes.tabDropdownMenuTitle} primary="Note"/>
          {openNote? <ExpandLess/> : <ExpandMore/>}
      </ListItemButton>
      <Collapse in={openNote} timeout="auto" unmountOnExit>
      <div style={{padding:"30px"}}>
          <TextField
              // sx={{marginLeft:"10px", marginRight:"30px"}}
              fullWidth
              id="outlined-multiline-flexible"
              label="Brew note"
              multiline
              maxRows={4}
              value={value}
              onChange={handleChange}
          />
      </div>
          <Grid className={classes.marginLeftNoteButton} item xs={0}>
              < AddCircleOutlineIcon className="fa fa-plus-circle marginTop" sx={{color: '#3d84a8'}} onClick={postNoteToApi}/>
          </Grid>
          <Card className={classes.noteResultCard} >
              <CardContent>{recipeNote}</CardContent>
          </Card >
      </Collapse>
      </>
  )
}
