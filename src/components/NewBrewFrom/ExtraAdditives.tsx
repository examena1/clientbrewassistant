import React, {useEffect, useState} from 'react';
import {
    Card,
    CardContent,
    Collapse,
    Grid,
    ListItemButton,
    ListItemText,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    TextField
} from "@mui/material";
import {useStyles} from "../../Style";
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import {ExpandLess, ExpandMore} from "@material-ui/icons";
import {IAddetive, IAddetivePost} from "../../Interface/IAddetive";
import {apiHandler} from "../../ApiHandler/ApiHandler";

export const ExtraAdditives = () => {
    const classes = useStyles();
    const [additive, setAdditive] = useState<IAddetivePost>({
        additive: "",
        batchNr: "",
        property: "",
        gram: 0,
    })
    const [additiveList, setAdditiveList] = useState<IAddetive[]>([])
    const [openAdditives, setOpenAdditives] = React.useState(false);
    const [id, setId] = useState<string>(JSON.parse(String(sessionStorage.getItem("recipe"))));
    const handleClickOne = () => {
        setOpenAdditives(!openAdditives);
    };



    const getAdditivesListFromApi = () => {
        apiHandler.get<IAddetive[]>('http://localhost:8080/additive/all-additives-from-recipe/' + id).then(additive => setAdditiveList(additive));
    }
    const postAdditiveToApi = () => {

        apiHandler.post<IAddetivePost>('http://localhost:8080/additive/' + id, additive)
            .then(data => data).catch(res => res).then(() => getAdditivesListFromApi());
    }

    const deleteAdditiveFromListApi = (id: string) => {
        apiHandler.delete('http://localhost:8080/additive/' + id).catch(i => i).then(() => getAdditivesListFromApi());
    }

    return (
        <>

            <ListItemButton onClick={handleClickOne}>
                <img className={classes.iconImage} src={require("../../icons/4773225.png")} alt=""/>
                <ListItemText className={classes.tabDropdownMenuTitle} primary="Additives"/>
                {openAdditives ? <ExpandLess/> : <ExpandMore/>}
            </ListItemButton>
            <Collapse in={openAdditives} timeout="auto" unmountOnExit>
                <Card sx={{minWidth: 275}}>
                    <CardContent className={classes.fontS}>
                        <TableContainer component={Paper}>
                            <Table aria-label="simple table">
                                <TableHead>
                                    <TableRow>
                                        <TableCell>Additive</TableCell>
                                        <TableCell align="left">Batch nr</TableCell>
                                        <TableCell >Property</TableCell>
                                        <TableCell >Gram</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {additiveList.map((item, index) => (
                                        <TableRow onDoubleClick={() => deleteAdditiveFromListApi(item.id)} key={index}>
                                            <TableCell component="th" scope="row">{item.additive}</TableCell>
                                            <TableCell align="left">{item.batchNr}</TableCell>
                                            <TableCell >{item.property}</TableCell>
                                            <TableCell >{item.gram}</TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </TableContainer>
                        <Grid
                            className={classes.marginForTables}
                            container
                            direction="row"
                            justifyContent="space-around"
                            alignItems="center"
                        >
                            <Grid item xs={2}>
                                <TextField id="standard-basic" label="Additives" variant="standard"
                                           onChange={e => setAdditive(val => ({...val, additive: e.target.value}))}/>
                            </Grid>
                            <Grid item xs={2}>
                                <TextField id="standard-basic" label="Batch nr" variant="standard"
                                           onChange={e => setAdditive(val => ({...val, batchNr: e.target.value}))}/>
                            </Grid><Grid item xs={2}>
                            <TextField id="standard-basic" label="Property" variant="standard"
                                       onChange={e => setAdditive(val => ({...val, property: e.target.value}))}/>
                        </Grid>
                            <Grid item xs={2}>
                                <TextField id="standard-basic" label="Gram" variant="standard"
                                           onChange={e => setAdditive(val => ({
                                               ...val,
                                               gram: parseInt(e.target.value)
                                           }))}/>
                            </Grid>
                            <Grid item xs={0}>
                                < AddCircleOutlineIcon
                                    onClick={postAdditiveToApi}
                                    className="fa fa-plus-circle marginTop" sx={{color: '#3d84a8'}}/>
                            </Grid>
                        </Grid>
                    </CardContent>
                </Card>
            </Collapse>
        </>
    )
}
