import React, {FC,useRef} from "react";
import {ColorHex, CountdownCircleTimer} from 'react-countdown-circle-timer'

// https://www.npmjs.com/package/react-native-countdown-circle-timer
interface TimeValProps{
    setTime:number,
    label:string,
    color:ColorHex
    index:number;
}

export const  TimerChild:FC<TimeValProps> = ({setTime,label,color,index}) => {

    const getTimeMinutes = (time: number) => (((time % setTime) / 60) | 0);


    const renderTime = (dimension:string, time:number) => {
        return (
            <div  className="time-container">
                <div style={{color}} className="time-wrapper">
                    <div className="time">{time}</div>
                    <div>{dimension}</div>
                </div>
            </div>
        );
    };

    const audioRef = useRef<HTMLAudioElement>(null!)

    return  (
        <div>
            <audio ref={audioRef}>
                <source src={"/alarmtwo.mp3"} />
            </audio>
            <CountdownCircleTimer
                isPlaying
                duration={setTime}
                size={95}
                strokeWidth={3}
                colors={color}
                onUpdate={(update)=>{
                }}

                onComplete={(totalElapsedTime) => {
                       audioRef.current.play()
                }}
            >
                {({ remainingTime }) =>(
                    renderTime(label ,getTimeMinutes(remainingTime))
                    )}
            </CountdownCircleTimer>
        </div>
    )
}
