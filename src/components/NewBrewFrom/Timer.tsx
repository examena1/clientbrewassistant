import React, {FC, useContext, useEffect, useState} from "react";
import {ColorHex} from "react-countdown-circle-timer";
import {
    Button,
    Collapse,
    Grid,
    ListItemButton,
    ListItemText,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow
} from "@mui/material";
import Box from "@mui/material/Box";
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";
import {ExpandLess, ExpandMore} from "@material-ui/icons";
import {useStyles} from "../../Style";
import {ITimer, ITimerPost} from "../../Interface/ITimer";
import {apiHandler} from "../../ApiHandler/ApiHandler";
// import {UserIdsContext} from "../context/UserIdsContext";


export const Timer: FC = () => {
    const classes = useStyles();
    const [timer, setTimerObj] = useState<ITimerPost>({
        color: "#3d84a8",
        title: "",
        time: 0,
        number: 0
    })

    const [openTimer, setTimer] = useState(false);
    const [timerList, setTimerList] = useState<ITimer[]>([])
    const [id, setId] = useState<string>(JSON.parse(String(sessionStorage.getItem("recipe"))));

    {/* denna lista är tänkt ska hämtas från api kallelse backend, kommer ändras i framtiden  */}
    const [numberVal,setNumberVal]=useState<number[]>([
        0,1,2,3,4,5,6,7,8,9,10,
        11,12,13,14,15,16,17,18,19,
        20,21,22,23,24,25,26,27,28,29,
        30,31,32,33,34,35,36,37,38,39,
        40,41,42,43,44,45,46,47,48,49,
        50,51,52,53,54,55,56,57,58,59,
        60,61,62,63,64,65,66,67,68,69,
        70,71,72,73,74,75,76,77,78,79,
        80,81,82,83,84,85,86,87,88,89,90,]);


    const deleteTimerFromListApi = (id: string) => {
        apiHandler.delete('http://localhost:8080/timer/' + id).catch(i => i).then(() => getTimerListFromApi());
    }

    const postTimerToApi = () => {
        apiHandler.post<ITimerPost>('http://localhost:8080/timer/'+id, timer)
            .then(data => console.log(data)).catch(res => res).then(() => getTimerListFromApi());
    }

    const getTimerListFromApi = () => {
        apiHandler.get<ITimer[]>('http://localhost:8080/timer/all-timers-from-recipe/'+ id).then(timer => setTimerList(timer));
    }
    const handleClickFive = () => {
        setTimer(!openTimer);
    };

    const setNumberAndPostObject=()=>{
        setTimerObj(val => ({...val, number: val.number +1}));
        postTimerToApi();
    }


    return (
        <>
            <ListItemButton onClick={handleClickFive}>
                <img className={classes.iconImage} src={require("../../icons/771444.png")} alt=""/>
                <ListItemText className={classes.tabDropdownMenuTitle} primary="Timer"/>
                {openTimer ? <ExpandLess/> : <ExpandMore/>}
            </ListItemButton>
            <Collapse in={openTimer} timeout="auto" unmountOnExit>
                <Grid className="colorClass" container justifyContent="space-around" spacing={0}>
                    <Grid item xs={3} sm={3} md={3}>
                        <Box sx={{minWidth: 120}}>
                            <FormControl fullWidth>
                                <InputLabel id="demo-simple-select-label">Color</InputLabel>
                                <Select
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select"
                                    onChange={e => setTimerObj(val => ({...val, color: e.target.value as ColorHex}))}
                                >
                                    <MenuItem value={'#9ee8c2'}>Light green</MenuItem>
                                    <MenuItem value={'#f2c288'}>Light orange</MenuItem>
                                    <MenuItem value={'#e663cb'}>Pink</MenuItem>
                                </Select>
                            </FormControl>
                        </Box>
                    </Grid>
                    <Grid item xs={3} sm={3} md={3}>
                        <Box sx={{minWidth: 120}}>
                            <FormControl fullWidth>
                                <InputLabel id="demo-simple-select-label">Timer Title</InputLabel>
                                <Select
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select"
                                    onChange={e => setTimerObj(val => ({...val, title: e.target.value as string}))}
                                >
                                    <MenuItem value={'Boil time'}>Boil time</MenuItem>
                                    <MenuItem value={'Add Hops'}>Add Hops</MenuItem>
                                    <MenuItem value={'whirlpool'}>whirlpool</MenuItem>
                                    <MenuItem value={'Additives'}>Other</MenuItem>
                                </Select>
                            </FormControl>
                        </Box>
                    </Grid>
                    <Grid className="position" item xs={3} sm={3} md={3}>
                        <Box sx={{minWidth: 120}}>
                            <FormControl fullWidth>
                                <InputLabel id="demo-simple-select-label">Set minuts</InputLabel>
                                <Select
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select"
                                    onChange={e => setTimerObj(val => ({...val, time: e.target.value as number}))}
                                >
                                   {
                                        numberVal.map(item=>{
                                            return   <MenuItem value={item*60}>{item}</MenuItem>
                                        })
                                    }


                                </Select>
                            </FormControl>
                        </Box>
                    </Grid>
                    <Grid item xs={1} sm={1} md={1}>
                        <Button className="position" onClick={setNumberAndPostObject} variant="text">
                            < AddCircleOutlineIcon fontSize="large" className="marginTop"/>
                        </Button>
                    </Grid>
                </Grid>
                <Grid
                    container
                    direction="row"
                    justifyContent="space-around"

                >
                    <TableContainer component={Paper}>
                        <Table aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                    <TableCell>Minuts</TableCell>
                                    <TableCell align="right">Title</TableCell>
                                    <TableCell align="right">Color</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {timerList.map((item, index) => (
                                    <TableRow onDoubleClick={() => deleteTimerFromListApi(item.id)} key={index}>
                                        <TableCell component="th" scope="row">{item.time / 60}</TableCell>
                                        <TableCell align="right">{item.title}</TableCell>
                                        <TableCell align="right">{item.color}</TableCell>

                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Grid>
            </Collapse>
        </>
    );
}
