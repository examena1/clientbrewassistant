import React, {useState} from 'react';
import {
    Card,
    CardContent,
    Collapse,
    Grid,
    ListItemButton,
    ListItemText,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    TextField
} from "@mui/material";
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";
import {useStyles} from "../../Style";
import {ExpandLess, ExpandMore} from "@material-ui/icons";
import {IHops, IHopsPost} from "../../Interface/IHops";
import {apiHandler} from "../../ApiHandler/ApiHandler";
import {IVolume} from "../../Interface/IVolume";

export const Hops = () => {
    const classes = useStyles();
    const [hops,setMainHops] =useState<IHopsPost>(
        {
            hops: "",
            batchNr: "",
           alphaAcid: 0,
            parts: 0,
          });
    const [hopsList,setMainHopsList] =useState<IHops[]>([]);
    const [totalHopsInList,setTotalHopsInList] = useState<number>(0);
    const [volume,setVolume] = useState<number>(0);
    const [openHops, setHops] = React.useState(false);
    const handleClickThree = () => {setHops(!openHops);};
    const [id, setId] = useState<string>(JSON.parse(String(sessionStorage.getItem("recipe"))));

    const getNewVolumeApi=()=>{
        apiHandler.get<IVolume>('http://localhost:8080/malt/totalmaltgrams/'+id).then(vol => setVolume(vol.newVolume));
    }

    const getHopsListFromApi = () => {
        apiHandler.get<IHops[]>('http://localhost:8080/hops/all-hops-from-recipe/'+id).then(hops=>setMainHopsList(hops)).then(()=>getTotalHopsFromHopsList());
    }
    const deleteHopsFromListApi = (id:string) => {
        apiHandler.delete('http://localhost:8080/hops/'+id).catch(i=>i).then(()=> getHopsListFromApi()).then(()=>getTotalHopsFromHopsList());
    }

    const getTotalHopsFromHopsList = () => {
        apiHandler.get<number>('http://localhost:8080/hops/totalhopsgrams/'+id).then(hops=>setTotalHopsInList(hops));
    }
    const  postHopsToApi=()=>{
        getNewVolumeApi();
        volume<0?alert("Total Volume must be reater then 0 "):
        apiHandler.post<IHopsPost>('http://localhost:8080/hops/'+id,hops)
            .then(data=>console.log(data)).catch(res=>res).then(()=>getHopsListFromApi()).then(()=>getTotalHopsFromHopsList());
    }

    return(
      <>
          <ListItemButton onClick={handleClickThree}>
              <img   className={classes.iconImage }src={require("../../icons/539858.png")} alt=""/>
              <ListItemText className={classes.tabDropdownMenuTitle} primary="Hops"/>
              {openHops ? <ExpandLess/> : <ExpandMore/>}
          </ListItemButton>
          <Collapse in={openHops} timeout="auto" unmountOnExit>
      <Card sx={{minWidth: 275}}>

          <CardContent className={classes.fontS}>
              <TableContainer component={Paper}>
                  <Table aria-label="simple table">
                      <TableHead>
                          <TableRow>
                              <TableCell>Hops</TableCell>
                              <TableCell align="right">Batch nr</TableCell>
                              <TableCell align="right">Alpha acid%</TableCell>
                              <TableCell align="right">Parts %</TableCell>
                              <TableCell align="right">Total hops: {totalHopsInList}</TableCell>
                          </TableRow>
                      </TableHead>
                      <TableBody>
                          {hopsList.map((item,index) => (
                              <TableRow  onDoubleClick={() => deleteHopsFromListApi(item.id)} key={index}>
                                  <TableCell component="th" scope="row">{item.hops}</TableCell>
                                  <TableCell align="right">{item.batchNr}</TableCell>
                                  <TableCell align="right">{item.alphaAcid}</TableCell>
                                  <TableCell align="right">{item.parts}</TableCell>
                                  <TableCell align="right">{item.gram}</TableCell>
                              </TableRow>
                          ))}
                      </TableBody>
                  </Table>
              </TableContainer>
              <Grid
                  container
                  direction="row"
                  justifyContent="space-around"
                  alignItems="center"
              >
                  <Grid item xs={2}>
                      <TextField id="standard-basic" label="Hops" variant="standard" onChange={e=>setMainHops(val =>({...val, hops: e.target.value}))}/>
                  </Grid>
                  <Grid item xs={2}>
                      <TextField id="standard-basic" label="Batch nr" variant="standard" onChange={e=>setMainHops(val =>({...val, batchNr: e.target.value}))}/>
                  </Grid><Grid item xs={2}>
                  <TextField id="standard-basic" label="Alpha acid" variant="standard" onChange={e=>setMainHops(val =>({...val, alphaAcid:parseInt( e.target.value)}))}/>
              </Grid>
                  <Grid item xs={2}>
                      <TextField id="standard-basic" label="Parts %" variant="standard" onChange={e=>setMainHops(val =>({...val, parts:parseInt( e.target.value)}))} />
                  </Grid>
                  <Grid item xs={0}>
                      < AddCircleOutlineIcon className="fa fa-plus-circle marginTop" sx={{color: '#3d84a8'}}
                                             onClick={postHopsToApi}
                      />
                  </Grid>

              </Grid>
          </CardContent>
      </Card>
</Collapse>
    </>
  )
}
