import React, {useEffect, useState} from 'react';
import logo from '../../images/two.png'
import altLogo from '../../images/one.png'
import {Button, Grid} from "@mui/material";
import {Recipes} from "../Recipes/Recipes";
import {useNavigate} from 'react-router-dom';
import {apiHandler} from "../../ApiHandler/ApiHandler";


export const Home = (props: any) => {
    const navigate = useNavigate();
    const [showList, setShowList] = useState<boolean>(false)
    // const [id, setId] = useState<string>(JSON.parse(String(sessionStorage.getItem("user"))));
    const [id, setId] = useState<string>("");


    useEffect(()=>{

        if (sessionStorage.getItem("user")===null||sessionStorage.getItem("user")==undefined){
            setId("");

        }else {
            setId(JSON.parse(String(sessionStorage.getItem("user"))))
        }

    },[])

    const reloadPage = () => {

        window.location.reload()

    }



    const hideAndShow = () => {
        setShowList(showList === false ? true : false)
    }
    const toCraterRecipe = () => {
        apiHandler.postPathVariable<any>('http://localhost:8080/recipe/create/'+id)
            .then(response => sessionStorage.setItem("recipe", JSON.stringify(response.id)))
            .then(()=>    navigate('/create'))
            .catch(data => console.log(data));

    }
    return (
        <div>
            <img id="logoHome" src={logo} alt={altLogo}/>
            <Grid
                container
                direction="row"
                justifyContent="space-around"
                alignItems="center"
                sx={{flexGrow: 1}}>
                {/*<StyledButton>*/}
                <Grid item xs={11}>
                    <Button onClick={toCraterRecipe} variant="outlined"
                            sx={{color: '#7cb3cf', border: '1px solid #7cb3cf', marginBottom: "5px"}}>New Brew</Button>
                </Grid>
                <Grid item xs={11}>
                    <Button onClick={hideAndShow} variant="outlined"
                            sx={{color: '#7cb3cf', border: '1px solid #7cb3cf'}}>You're previous brews</Button>
                </Grid>
                {/*</StyledButton>*/}
            </Grid>
            {showList ? <Recipes/> : ""}
        </div>
    )
}

