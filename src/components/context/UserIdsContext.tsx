import React, {createContext, useEffect, useState} from "react";

type contextProviderProps={
    children: React.ReactNode
}
type UserIds={
    userId:string,
    recipeId:string

}
export const  UserIdsContext= createContext<UserIds>({recipeId: "", userId: ""})

export const UserIdsContextProvider =({children}:contextProviderProps)=>{

    const [userIDs,setUserIDs]=useState<UserIds>({recipeId: "", userId: ""})

    useEffect(() => {
        // setUserIDs({
        //     // recipeId: JSON.parse(String(  sessionStorage.getItem('recipe'))),
        //     // userId:JSON.parse(String(  sessionStorage.getItem('user')))
        // })
    },[])
    return <UserIdsContext.Provider value={userIDs}>{children}</UserIdsContext.Provider>
}
