import {makeStyles} from "@mui/styles";


export const useStyles = makeStyles({
   imgNewBrewForm: {
        display: 'block',
        marginTop: '5px',
        marginRight:'auto',
        marginBottom:'40px',
        width: '70%'
     },
     input:{
        height:'40%'
     },
    fontS:{
       fontSize:'2.5vw'
    },
    iconImage:{
       maxWidth:'40px'
    },
    tabDropdownMenuTitle:{
       marginLeft:'10%',
    },
    datepickerButton:{
       margin:"0px",
    },
    marginForTables:{
       marginTop:'10px'
    },
    marginLeftNoteButton:{
       marginLeft:'10px'
    },
    noteResultCard:{
       marginLeft:'5%',
       marginRight:'5%',
        marginBottom:'2%'
    }

});
