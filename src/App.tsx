import React, {useEffect, useState} from 'react';
import './App.css';
import {RouterComponent} from "./Router/RouterComponnent";
import {useAuthenticator, withAuthenticator, Authenticator} from '@aws-amplify/ui-react'
import {apiHandler} from "./ApiHandler/ApiHandler";
import {IUser, IUserCreate} from './Interface/IUser';
import Amplify, {Hub, Logger} from 'aws-amplify';
import {Auth} from 'aws-amplify'


const App = (props: any) => {

    const [userEmail, setUserEmail] = useState<any>(props.user.attributes.email)

    useEffect(() => {
        console.log(userEmail);
        console.log(props);

     console.log(props.user.attributes.email_verified);

     if (props.user.attributes.email_verified){
        getExistingUser();
        }else {
         props.signOut()
     }
    }, [])


    //Denna kod fungerar ej som de ska. även inom useEffect ,låter ligga vill lösa de efter examen, just nu är den good enough stadie
    Hub.listen("auth", (event: any) => {

        switch (event.payload.event) {

            case 'signIn':
                getExistingUser();
                console.log("Signed in  funct", event.payload.event)
                break;
            case 'signUp':
                createUser();
                break;
            case 'signOut':
                console.log('user signed out ', event);
                break;
            case 'signIn_failure':
                console.log('user sign in failed ', event);
                break;
            case 'configured':
                console.log('the Auth module is configured ', event);
        }
    });




    const getExistingUser = () => {
        apiHandler.get<IUser>('http://localhost:8080/user/email/' +  userEmail)
            .then(response => {
                if (response.status === 500) {
                    createUser();
                } else {
                    sessionStorage.setItem("user", JSON.stringify(response.id))
                }
            })
            .catch(err => console.log(err))
    }


    const createUser = () => {
        apiHandler.post<IUserCreate>('http://localhost:8080/user/create', {email: userEmail})
            .then(data => sessionStorage.setItem("user", JSON.stringify(data.id)))
            .catch(err => console.log(err))
    }


    return (
        <div className="App">

            < RouterComponent/>
        </div>
    );
}

export default withAuthenticator(App);
