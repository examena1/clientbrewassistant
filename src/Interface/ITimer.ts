import {ColorHex} from "react-countdown-circle-timer";

export interface ITimer {
    id: string;
    color: ColorHex;
    title:string;
    time:number;
    number:number;
}
export interface ITimerPost {
    color: ColorHex;
    title:string;
    time:number;
    number:number;
}
