export interface IMashVolume {
    id?: string;
    totalMalt: number;
    mashVolume:number
    lauteringWater:number;
}
