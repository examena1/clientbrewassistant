export interface IVolume {
    breBoil:number;
    afterBoil:number;
    gravityAfterBoiling:number;
    newVolume:number
}
