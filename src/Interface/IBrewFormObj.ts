export interface IBrewFormObj {
    date:Date|null;
    beerName:string;
    beerStyle:string;
    batchNr:string;
    goalIbu:number;
    expectedGravity:number;

}
// public String beerName;
// public String beerStyle;
// public String batchNr;
// public Double goalIbu;
// public Double expectedGravity;