export interface IHops {
    id: string;
    hops: string;
    batchNr: string;
    alphaAcid: number;
    parts: number;
    gram: number;

}
export interface IHopsPost {
    hops: string;
    batchNr: string;
    alphaAcid: number;
    parts: number;
}
