import {IBrewFormObj} from "./IBrewFormObj";
import {IAddetive} from "./IAddetive";
import {IMalt} from "./IMalt";
import {IVolume} from "./IVolume";
import {ITimer} from "./ITimer";
import {IMashVolume} from "./IMashVolume";
import {IHops} from "./IHops";

export  interface IBrewRecipe {
    id:string;
    beerValues: IBrewFormObj;
    mashVolume: IMashVolume
    additiveList:IAddetive[];
    maltList:IMalt[];
    hopsList:IHops[];
    volume:IVolume;
    timerList: ITimer[];
    note:string;
}

// public BeerValuesEntity beerValues;
// public MashEntity mashVolume;
// public List<AdditiveEntity> additives;
// public List<MaltEntity> malts;
// public List<HopsEntity> hops;
// public VolumeEntity volumeEntity;
// public List<TimerEntity> timers;
// public String note;
