import {IBrewRecipe} from "./IBrewRecipe";

export interface IUser {
    status: number;
    id: string;
    email:string;
    // recipes:IBrewRecipe []
}
export interface IUserCreate {
    id: string;
    email:string;
    // recipes:IBrewRecipe []
}

export interface IPostUser {
    email:string;
}
