export interface IAddetive {
    id: string;
    additive:string;
    batchNr:string;
    property:string;
    gram:number;
}
export interface IAddetivePost {

    additive:string;
    batchNr:string;
    property:string;
    gram:number;
}
