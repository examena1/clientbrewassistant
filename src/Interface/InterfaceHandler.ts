import React, {useState, useEffect, FC} from "react";

export interface TestInterface {
    id?: number;
    text: string;
    day: string;
    reminder: boolean;
}

export interface AdditivesInterface {
    Additive: string;
    BatchNr: string;
    Property: string;
    gram: number;
}

