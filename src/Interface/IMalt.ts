export interface IMalt {
    id: string;
    malt:string;
    batchNr:string;
    parts:number;
    gram:number;

}
export interface IMaltPost {
    malt:string;
    batchNr:string;
    parts:number;
}
