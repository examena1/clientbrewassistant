import {IMalt} from "./IMalt";
import {IMashVolume} from "./IMashVolume";

interface IMaltAndMash {
    malt:IMalt;
    mash:IMashVolume;

}
