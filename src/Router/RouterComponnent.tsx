import React from 'react';
import {BrowserRouter, Route, Routes} from "react-router-dom";
import {Home} from "../components/Home/Home";
import {NewBrewForm} from "../components/NewBrewFrom/NewBrewForm";
import {Recipe} from "../components/CreatedRecipe/Recipe";
import {Recipes} from "../components/Recipes/Recipes";
import {ButtonAppBar} from "../components/ButtonNavbar/ButtonAppBar";
import {RecipeInList} from "../components/CreatedRecipe/RecipeInList";


export  const RouterComponent =()=>{
    return(
        <BrowserRouter >
            <ButtonAppBar />
            <Routes>

                {/*<Route path="/" element={<App />} />*/}
                <Route path="/" element={<Home />} />
                <Route path="/create" element={<NewBrewForm />} />
                <Route path="/inlist" element={<RecipeInList/>} />
                <Route path="/recipe" element={<Recipe />} />
                <Route path="/list" element={ <Recipes />} />
            </Routes>
        </BrowserRouter >
    );
}
